import shlex
import subprocess
from websocket import create_connection
import ssl
import json
import os
import sys
import boto3

key = os.environ['MPC_WS_KEY']
wss_uri = os.environ['MPC_WS_URI'] + '?key=' + key
report_status_function_name = os.environ['MPC_REPORT_STATUS_FUNCTION_NAME']


def start_player(username, player_index, task_id):
    try:
        ws = create_connection(wss_uri, sslopt={"cert_reqs": ssl.CERT_NONE})
    except:
        print("connecting to websocket failed")
    cmd_array = shlex.split('./Player.x ' + str(player_index) + ' Programs/tutorial')
    print("task id: {}".format(task_id))
    print(cmd_array)
    process = subprocess.Popen(cmd_array, stdout=subprocess.PIPE)
    status = False
    while True:
        output = process.stdout.readline()
        if not output:
            print('exiting...')
            break
        if output:
            message = output.strip().decode("utf-8")
            print(message)
            if 'End of prog' in message:
                status = True
            try:
                ws.send(
                    json.dumps(
                        [
                            json.dumps(
                                {
                                    'msg': message
                                }
                            )
                        ]
                    )
                )
                result = ws.recv()
                print('response: ' + result)
            except:
                print("send message to websocket failed")
    rc = process.poll()
    ws.close()
    player_id = 'Player' + str(player_index + 1)
    response = report_status(task_id, username, player_id, status)
    print(response)
    return rc


def report_status(task_id, username, player_id, is_successful):
    print("reporting status")
    client = boto3.client('lambda')
    event = {
        'taskId': task_id,
        'username': username,
        'playerId': player_id,
        'isSuccessful': is_successful
    }
    response = client.invoke(
        FunctionName=report_status_function_name,
        InvocationType='Event',
        Payload=json.dumps(event)
    )
    return response


def main():
    if len(sys.argv) < 4:
        print('please provide the arguments in order: username, player_sequence_number, task_id')
        return 1
    if not str.isdigit(sys.argv[2]):
        print('player number must be an integer')
        return 1
    username = sys.argv[1]
    player_sequence_number = int(sys.argv[2])
    task_id = sys.argv[3]
    return start_player(username, player_sequence_number, task_id)


if __name__ == "__main__":
    main()
